Name:    blktrace
Version: 1.3.0
Release: 3
Summary: Block IO tracer in the Linux kernel
License: GPLv2+
Source:  http://brick.kernel.dk/snaps/blktrace-%{version}.tar.bz2
URL:     https://git.kernel.dk/cgit/blktrace 

BuildRequires: gcc,  xz, libaio-devel, python3, librsvg2-devel blktrace sysstat theora-tools
Provides:      iowatcher = %{version}-%{release}
Obsoletes:     iowatcher < %{version}-%{release}
Requires:      python3

Patch1:  0001-blktrace-remove-python2-dedpendency.patch
Patch2:  0002-blktrace-Makefile-add-fstack-protector-strong-flag.patch
Patch3:  0003-blktrace-fix-exit-directly-when-nthreads-running.patch
Patch4:  0004-blkparse-skip-check_cpu_map-with-pipe-input.patch
Patch5:  0005-blkparse-fix-incorrectly-sized-memset-in-check_cpu_m.patch
Patch6:  0006-fix-hang-when-BLKTRACESETUP-fails-and-o-is-used.patch

%description
blktrace is a block layer IO tracing mechanism which provides detailed
information about request queue operations up to user space. This is
valuable for diagnosing and fixing performance or application problems
relating to block layer io.

%package help
Summary: Including man files for blktrace
Requires: man

%description    help
This contains man files for the using of blktrace.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%make_build

%install
make dest=%{buildroot} prefix=%{buildroot}/%{_prefix} mandir=%{buildroot}/%{_mandir} install

%files
%defattr(-,root,root)
%doc README COPYING
%{_bindir}/*
%{_bindir}/iowatcher


%files help
%{_mandir}/man1/*
%{_mandir}/man8/*


%changelog
* Wed Jul 31 2024 wangzhiqiang <wangzhiqiang95@huawei.com> - 1.3.0-3
- backport bugfix patches from upstream

* Mon May 16 2022 Li Jinlin <lijinlin3@huawei.com> - 1.3.0-2
- provide iowatcher via Provides

* Mon Nov 22 2021 Li Jinlin <lijinlin3@huawei.com> - 1.3.0-1
- Update blktrace version to 1.3.0-1

* Sat Oct 09 2021 zhanchengbin <zhanchengbin1@huawei.com> - 1.2.0-26
- Fixed the issue of modifying parallel compilation

* Wed Sep 29 2021 Wenchao Hao <haowenchao@huawei.com> - 1.2.0-25
- NOP:nothing but to make it able to sync between differnt branches

* Mon Sep 13 2021 zhanchengbin <zhanchengbin1@huawei.com> - 1.2.0-21
- DESC: Makefile add fstack-protector-srtong flag patch

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.0-20
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Jul 23 2021 yanglongkang <yanglongkang@huawei.com> - 1.2.0-19
- delete -j1 compilation options

* Fri Jul 23 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.2.0-18
- remove useless buildrequires

* Fri Jul 02 2021 linxiaoran <linxiaoran@huawei.com> - 1.2.0-17
- Fix blktrace exit patch

* Thu Sep 10 2020 lihaotian <lihaotian9@huawei.com> - 1.2.0-16
- create iowatcher rpm sub-package

* Sun Jul 12 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1.2.0-15
- backport upstream patches

* Wed Jul 1 2020 Wu Bo <wubo009@163.com> - 1.2.0-14
- rebuild package

* Wed Mar 18 2020 sunshihao<sunshihao@huawei.com> - 1.2.0-13
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESCi:modify python2 expression to python3

* Tue Feb 25 2020 hy-euler <eulerstoragemt@huawei.com> - 1.2.0-12
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:modify the URL to git web in spec file

* Mon Feb 17 2020 sunshihao<sunshihao@huawei.com> - 1.2.0-11
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:blktrace remove python2 dependency

* Fri Aug 30 2019 zoujing<zoujing13@huawei.com> - 1.2.0-10
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:openEuler Debranding

* Thu Aug 15 2019 Buildteam <buildteam@openeuler.org> - 1.2.0-9
- Package Initialization

